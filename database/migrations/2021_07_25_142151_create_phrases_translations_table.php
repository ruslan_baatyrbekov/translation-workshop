<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhrasesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrases_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('phrases_id')->constrained()->cascadeOnDelete();
            $table->string('locale')->index();
            $table->string('phrase');
            $table->unique(['phrases_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrases_translations');
    }
}
