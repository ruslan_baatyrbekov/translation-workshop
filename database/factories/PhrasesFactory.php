<?php

namespace Database\Factories;

use App\Models\Phrases;
use App\Models\PhrasesTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhrasesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Phrases::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        $de_faker = \Faker\Factory::create('de_DE');
        $fr_faker = \Faker\Factory::create('fr_FR');
        $it_faker = \Faker\Factory::create('it_IT');
        return array(
            [
                'en' => [
                'phrase' => $this->faker->paragraph
                ],
                'de' => [
                'phrase' => $de_faker->paragraph
                ],
                'fr' => [
                'phrase' => $fr_faker->paragraph
                ],
                'it' => [
                'phrase' => $it_faker->paragraph
                ],
            ]);
    }
}
