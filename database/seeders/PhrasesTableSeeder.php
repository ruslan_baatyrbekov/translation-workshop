<?php

namespace Database\Seeders;

use App\Models\Phrases;
use App\Models\User;
use Illuminate\Database\Seeder;

class PhrasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ru_faker = \Faker\Factory::create('ru_RU');
        $de_faker = \Faker\Factory::create('de_DE');
        $fr_faker = \Faker\Factory::create('fr_FR');
        $it_faker = \Faker\Factory::create('it_IT');
        $en_faker = \Faker\Factory::create('en_EN');
        $user1 = User::first();
        for($i = 0; $i < 4;$i++){
            $data = [
                'user_id' => $user1->id,
                'ru' =>
                    [
                        'phrase' => $ru_faker->name,
                    ]
            ];
            Phrases::create($data);
        }
        $users = User::all();
        for ($i = 1; $i < count($users);$i++){
            if($users[$i] !== 1)
            $data = [
                'en' =>
                    [
                        'phrase' => $en_faker->paragraph,
                    ],
                'de' =>
                    [
                        'phrase' => $de_faker->paragraph,
                    ],
                'fr' =>
                    [
                        'phrase' => $fr_faker->paragraph,
                    ],
                'it' =>
                    [
                        'phrase' => $it_faker->paragraph,
                    ]
            ];
            $phrase = Phrases::findOrFail($i);
            $phrase->update($data);
        }
    }
}
