<?php

namespace App\Http\Controllers;

use App\Models\Phrases;
use App\Models\PhrasesTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhrasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $phrases = Phrases::all();
        return view('phrases.index', compact('phrases'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = [
            'user_id' => $user->id,
            'ru' =>
                [
                'phrase' => $request->input('phrase_ru'),
                ]
        ];
        Phrases::create($data);
        return redirect()->route('/');
    }

    public function store_all(Request $request)
    {
        if(!empty($request->input('phrase_en'))){
            $data = [
                'en' =>
                    [
                        'phrase' => $request->input('phrase_en'),
                    ]
            ];
            $phrase = Phrases::findOrFail($request->input('phrase_id'));
            $phrase->update($data);
        }
        if(!empty($request->input('phrase_de'))){
            $data = [
                'de' =>
                    [
                        'phrase' => $request->input('phrase_de'),
                    ]
            ];
            $phrase = Phrases::findOrFail($request->input('phrase_id'));
            $phrase->update($data);
        }
        if(!empty($request->input('phrase_fr'))){
            $data = [
                'fr' =>
                    [
                        'phrase' => $request->input('phrase_fr'),
                    ]
            ];
            $phrase = Phrases::findOrFail($request->input('phrase_id'));
            $phrase->update($data);
        }
        if(!empty($request->input('phrase_it'))){
            $data = [
                'it' =>
                    [
                        'phrase' => $request->input('phrase_it'),
                    ]
            ];
            $phrase = Phrases::findOrFail($request->input('phrase_id'));
            $phrase->update($data);
        }
        return redirect()->back();
    }

    public function show(Phrases $phrase)
    {
        return view('phrases.show', compact('phrase'));
    }
}
