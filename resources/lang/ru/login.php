<?php

return [
    'login' => 'Вход',
    'email' => 'E-Mail Адресс',
    'password' => 'Пароль',
    'remember' => 'Запомнить меня?',
    'forgot' => 'Забыли свой пароль?',
];
