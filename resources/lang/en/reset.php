<?php

return [
    'reset' => 'Reset Password',
    'email' => 'E-Mail Address',
    'confirm' => 'Confirm Password',
    'password' => 'Password',
];
