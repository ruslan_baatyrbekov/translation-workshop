<?php

return [
    'main' => 'Translation workshop',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'LogOut',
];
