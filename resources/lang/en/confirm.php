<?php

return [
    'confirm' => 'Confirm Password',
    'confirm_pass' => 'Please confirm your password before continuing.',
    'password' => 'Password',
    'forgot' => 'Forgot Your Password?',
];
