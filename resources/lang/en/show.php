<?php

return [
    'translate' => 'Translation into different languages',
    'russian' => 'Phrase on russian',
    'english' => 'Enter phrase on english',
    'deutsch' => 'Enter phrase on deutsch',
    'france' => 'Enter phrase on france',
    'italian' => 'Enter phrase on italian',
    'trans_button' => 'Save translate'
];
