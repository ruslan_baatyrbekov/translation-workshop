@extends('layouts.app')

@section('content')
    @if(count($phrases) !== 0)
        <h1>@lang('index.phrases')</h1>
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('index.phrase_table')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($phrases as $phrase)
            <tr>
                <th scope="row">{{$phrase->id}}</th>
                <td><a href="{{route('phrases.show', ['phrase' =>$phrase])}}">{{$phrase->translate('ru')->phrase}}</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>

    @else
        @lang('index.no_phrase')
    @endif
    @if(\Illuminate\Support\Facades\Auth::user())
    <h2>@lang('index.add_phrase')</h2>
    <form method="post" action="{{route('phrases.store')}}">
        @csrf
        <div class="form-group">
            <label for="exampleInputPhraseRu">@lang('index.enter')</label>
            <input type="text" class="form-control" id="exampleInputPhraseRu" name="phrase_ru">
        </div>
        <button type="submit" class="btn btn-primary">@lang('index.enter_button')</button>
    </form>
    @endif
@endsection
