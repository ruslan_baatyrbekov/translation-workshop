@extends('layouts.app')

@section('content')
    <form method="post" action="{{route('phrases.store_all')}}">
        <h1>@lang('show.translate')</h1>
        @csrf
        <input type="hidden" name="phrase_id" value="{{$phrase->id}}">
        <div class="form-group">
            <label for="exampleInputPhraseRu">@lang('show.russian')</label>
            <input type="text" class="form-control" id="exampleInputPhraseRu" name="phrase_ru" value="{{$phrase->translate('ru')->phrase}}">
        </div>
        <div class="form-group">
            <label for="exampleInputPhraseEn">@lang('show.english')</label>
            <input type="text" class="form-control" id="exampleInputPhraseEn" name="phrase_en" @if($phrase->translate('en'))value="{{$phrase->translate('en')->phrase}}@endif">
        </div>
        <div class="form-group">
            <label for="exampleInputPhraseDe">@lang('show.france')</label>
            <input type="text" class="form-control" id="exampleInputPhraseDe" name="phrase_de" @if($phrase->translate('de'))value="{{$phrase->translate('de')->phrase}}@endif">
        </div>
        <div class="form-group">
            <label for="exampleInputPhraseFr">@lang('show.deutsch')</label>
            <input type="text" class="form-control" id="exampleInputPhraseFr" name="phrase_fr" @if($phrase->translate('fr'))value="{{$phrase->translate('fr')->phrase}}@endif">
        </div>
        <div class="form-group">
            <label for="exampleInputPhraseIt">@lang('show.italian')</label>
            <input type="text" class="form-control" id="exampleInputPhraseIt" name="phrase_it" @if($phrase->translate('it'))value="{{$phrase->translate('it')->phrase}}@endif">
        </div>

        <button type="submit" class="btn btn-primary">@lang('show.trans_button')</button>
    </form>
@endsection
