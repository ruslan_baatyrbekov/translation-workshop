<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware('language')->group(function(){
    Route::get('/', [\App\Http\Controllers\PhrasesController::class, 'index'])->name('/');
    Route::get('phrases.index',[\App\Http\Controllers\PhrasesController::class,'index'])->name('phrases.index');
    Route::get('phrases/{phrase}',[\App\Http\Controllers\PhrasesController::class,'show'])->name('phrases.show')->middleware('auth');
    Route::post('phrases.store',[\App\Http\Controllers\PhrasesController::class,'store'])->name('phrases.store');
    Route::post('phrases.store_all',[\App\Http\Controllers\PhrasesController::class,'store_all'])->name('phrases.store_all');
    Auth::routes();
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

Route::get('language/{locale}', [\App\Http\Controllers\LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')
    ->where('locale', 'ru|en');
