<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Phrases;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhrasesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection|User
     */
    private $another_user;

    /**
     * @var
     */
    private $phrase;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->another_user = User::factory()->create();
        $ru_faker = \Faker\Factory::create('ru_RU');
        $data = [
            'user_id' => $this->user->id,
            'ru' =>
                [
                    'phrase' => $ru_faker->name,
                ]
        ];
        $this->phrase = Phrases::create($data);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function index()
    {
        $response = $this->get('/');
        $response->assertSeeText(trans('index.phrases'));
        $response->assertSeeText(trans('index.phrase_table'));
        $response->assertSeeText(trans('index.no_phrase'));
        $response->assertSeeText(trans('index.add_phrase'));
        $response->assertSeeText(trans('index.enter'));
        $response->assertSeeText(trans('index.enter_button'));
        $response->assertStatus(200);
    }

    public function show()
    {
        $response = $this->get('phrases.show',['phrase' => $this->phrase]);
        $response->assertSeeText(trans('show.translate'));
        $response->assertSeeText(trans('show.russian'));
        $response->assertSeeText(trans('show.english'));
        $response->assertSeeText(trans('show.deutsch'));
        $response->assertSeeText(trans('show.france'));
        $response->assertSeeText(trans('show.italian'));
        $response->assertSeeText(trans('show.trans_button'));
        $response->assertStatus(200);
    }

    public function test_redirect_if_user_not_auth()
    {
        $response = $this->get(
            route('phrases.show', [
                'phrase' => $this->phrase
            ])
        );
        $response->assertRedirect(route('login'));
    }
}
