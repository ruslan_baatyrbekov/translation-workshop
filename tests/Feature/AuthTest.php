<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_not_auth()
    {
        $response = $this->get('/');
        $response->assertDontSeeText('Добавить фразу');
        $response->assertSeeText('Вход');
        $response->assertSeeText('Регистрация');
        $response->assertSeeText('Цех переводчиков');
        $response->assertStatus(200);
    }

    public function user_cannot_enter_phrases()
    {
        app()->setLocale('en');
        $response->assertSeeText('login');
        $response->assertSeeText('Register');
        $response->assertSeeText('Translation workshop');
    }
}
